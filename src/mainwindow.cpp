/*
    Copyright (c) 2015 - 2025, Vinari Software
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	   contributors may be used to endorse or promote products derived from
	   this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include "lib/mainwindow.h"
#include "ui_mainwindow.h"

int MainWindow::tiempoRetraso=1;

void MainWindow::saveSettings(){
	QSettings savedSettings("org.vinarisoftware", "powerdown");
	savedSettings.beginGroup("PowerDown");
	savedSettings.setValue("position", this->geometry());

	savedSettings.endGroup();
}

void MainWindow::loadSettings(){
	QSettings savedSettings("org.vinarisoftware", "powerdown");
	savedSettings.beginGroup("PowerDown");

    QRect defaultPos(250, 250, 400, 285);
    QRect ICRect=savedSettings.value("position", defaultPos).toRect();
    setGeometry(ICRect);

	MainWindow::tiempoRetraso=savedSettings.value("delay-time", 1).toInt();

	savedSettings.endGroup();
}

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	loadSettings();
	ui->pushButton_3->setEnabled(false);
	this->accionEstablecida=false;
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event){
	saveSettings();
	event->accept();
}

void MainWindow::on_actionSalir_triggered()
{
	QApplication::quit();
}

void MainWindow::on_actionAcerca_de_QT_triggered()
{
    QMessageBox::aboutQt(this, tr("PowerDown - Vinari Software | About QT"));
}

void MainWindow::on_actionAcerca_de_Vinari_OS_triggered()
{
    int questionReply=QMessageBox::question(this, "About Vinari OS", tr("You will be redirected to the Vinari OS website \nDo you want to proceed?"), QMessageBox::Yes|QMessageBox::No);

	if(questionReply==QMessageBox::Yes){
        QDesktopServices::openUrl(QUrl("http://vinarios.me"));
	}else{
		return;
	}
}

void MainWindow::on_actionAcerca_de_Vinari_Software_triggered()
{
    int questionReply=QMessageBox::question(this, "About Vinari Software", tr("You will be redirected to the Vinari Software website \nDo you want to proceed?"), QMessageBox::Yes|QMessageBox::No);

	if(questionReply==QMessageBox::Yes){
		QDesktopServices::openUrl(QUrl("https://vinarisoftware.wixsite.com/vinari"));
	}else{
		return;
	}
}

void MainWindow::on_pushButton_clicked()
{
	if(this->accionEstablecida==true){
        QMessageBox::warning(this, "PowerDown - Vinari Software", tr("There is already an scheduled action.\nPlease cancel it, and try again..."), QMessageBox::Ok);
		return;
	}

	QString commandToSend;
	#ifdef _WIN64
		commandToSend="shutdown -s -t ";
		commandToSend=commandToSend+QString::number(tiempoRetraso*60);
	#else
		commandToSend="shutdown -h +";
		commandToSend=commandToSend+QString::number(tiempoRetraso);
	#endif

	int exitCode=system(commandToSend.toStdString().c_str());
	if(exitCode!=0){
        QMessageBox::critical(this, "PowerDown - Vinari Software", tr("An error has occurred while trying to shutdown the computer...."), QMessageBox::Ok);
		return;
	}

    this->setWindowTitle(tr("Shuting down | PowerDown - Vinari Software"));
    ui->pushButton_3->setText(tr("Cancel shutdown"));

	ui->pushButton_3->setEnabled(true);
	this->accionEstablecida=true;
    QMessageBox::information(this, "PowerDown - Vinari Software", tr("The computer will shut down in ")+QString::number(tiempoRetraso)+tr(" minute(s)."), QMessageBox::Ok);
}

void MainWindow::on_pushButton_2_clicked()
{
	if(this->accionEstablecida==true){
        QMessageBox::warning(this, "PowerDown - Vinari Software", tr("There is already an scheduled action.\nPlease cancel it, and try again..."), QMessageBox::Ok);
		return;
	}

	QString commandToSend;
	#ifdef _WIN64
		commandToSend="shutdown -r -t ";
		commandToSend=commandToSend+QString::number(tiempoRetraso*60);
	#else
		commandToSend="shutdown -r +";
		commandToSend=commandToSend+QString::number(tiempoRetraso);
	#endif

	int exitCode=system(commandToSend.toStdString().c_str());
	if(exitCode!=0){
        QMessageBox::critical(this, "PowerDown - Vinari Software", tr("An error has occurred while trying to reboot the computer..."), QMessageBox::Ok);
		return;
	}

    this->setWindowTitle(tr("Rebooting | PowerDown - Vinari Software"));
    ui->pushButton_3->setText(tr("Cancel reboot"));

	ui->pushButton_3->setEnabled(true);
	this->accionEstablecida=true;
    QMessageBox::information(this, "PowerDown - Vinari Software", tr("The computer will reboot in ")+QString::number(tiempoRetraso)+tr(" minute(s)."), QMessageBox::Ok);
}

void MainWindow::on_pushButton_3_clicked()
{
	int exitCode;
	#ifdef _WIN64
		exitCode=system("shutdown -a");

	#elif __linux__
		exitCode=system("shutdown -c");
	#else
		exitCode=system("pkill shutdown");
	#endif

	if(exitCode!=0){
        QMessageBox::critical(this, "PowerDown - Vinari Software", tr("An error has occurred while trying to cancel the scheduled action..."), QMessageBox::Ok);
		return;
	}

	this->setWindowTitle("PowerDown - Vinari Software");
    ui->pushButton_3->setText(tr("Cancel"));

	ui->pushButton_3->setEnabled(false);
	this->accionEstablecida=false;
    QMessageBox::information(this, "PowerDown - Vinari Software", tr("The established action has been canceled successfully."), QMessageBox::Ok);
}

void MainWindow::on_actionConfiguraci_n_triggered()
{
	settingsWindow=new SettingsWindow(this);
	settingsWindow->show();
}

void MainWindow::on_actionAcerca_de_PowerDown_Vinari_Software_triggered()
{
	aboutWindow=new AboutWindow(this);
	aboutWindow->show();
}

void MainWindow::on_actionActivar_bot_n_cancelar_triggered()
{
    ui->pushButton_3->setEnabled(true);
    this->setWindowTitle(tr("Forced cancel | PowerDown - Vinari Software"));
}
