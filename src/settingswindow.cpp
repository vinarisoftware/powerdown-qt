/*
    Copyright (c) 2015 - 2025, Vinari Software
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	   contributors may be used to endorse or promote products derived from
	   this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include "lib/settingswindow.h"
#include "lib/mainwindow.h"
#include "ui_settingswindow.h"

void SettingsWindow::saveSettings(){

	bool rememberTime=ui->checkBox->isChecked();

	QSettings savedSettings("org.vinarisoftware", "powerdown");
	savedSettings.beginGroup("PowerDown");
	savedSettings.setValue("remember-time", rememberTime);

	if(rememberTime==false){
		savedSettings.setValue("delay-time", TiempoRetrasoAnterior);
	}else{
		savedSettings.setValue("delay-time", TiempoRetrasoAcutal);
	}
	savedSettings.endGroup();
}

void SettingsWindow::loadSettings(){
	QSettings savedSettings("org.vinarisoftware", "powerdown");
	savedSettings.beginGroup("PowerDown");
	ui->checkBox->setChecked(savedSettings.value("remember-time", true).toBool());

	savedSettings.endGroup();
}


SettingsWindow::SettingsWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::SettingsWindow)
{
	TiempoRetrasoAcutal=MainWindow::tiempoRetraso;
	ui->setupUi(this);
	ui->lineEdit->setText(QString::number(TiempoRetrasoAcutal));
	setAttribute(Qt::WA_DeleteOnClose);
	loadSettings();
}

void SettingsWindow::closeEvent(QCloseEvent *event){
	saveSettings();
	event->accept();
}

SettingsWindow::~SettingsWindow()
{
	delete ui;
}

void SettingsWindow::on_pushButton_clicked()
{
	QString buffer=ui->lineEdit->text();
	std::regex numericVerification("^[0-9 ]+$");

	TiempoRetrasoAnterior=TiempoRetrasoAcutal;

	if(std::regex_match(buffer.toStdString(), numericVerification)){
		TiempoRetrasoAcutal=buffer.toInt();
		if(TiempoRetrasoAcutal<400000){
			MainWindow::tiempoRetraso=TiempoRetrasoAcutal;
			this->close();
		}else{
			qDebug("Error: non-numeric values are not accepted!");
		}
	}
}

void SettingsWindow::on_pushButton_2_clicked()
{
    this->close();
}

