QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TRANSLATIONS = "PowerDown-VinariSoftware_es.ts"

RC_ICONS = myappico.ico
VERSION = 2.0.5
QMAKE_TARGET_COMPANY = "Vinari Software"
QMAKE_TARGET_PRODUCT = "PowerDown - Vinari Software"
QMAKE_TARGET_DESCRIPTION = "PowerDown - Vinari Software"
QMAKE_TARGET_COPYRIGHT = "2015 - 2024. Vinari Software. BSD License."
QT_FONT_DPI=96
QT_AUTO_SCREEN_SCALE_FACTOR=false
QT += widgets


DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000

SOURCES += \
    src/aboutwindow.cpp \
    src/licensewindow.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/settingswindow.cpp

HEADERS += \
    lib/aboutwindow.h \
    lib/licensewindow.h \
    lib/mainwindow.h \
    lib/settingswindow.h

FORMS += \
    ui/aboutwindow.ui \
    ui/licensewindow.ui \
    ui/mainwindow.ui \
    ui/settingswindow.ui

TRANSLATIONS += \
    PowerDown-VinariSoftware_es_ES.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    MyResources.qrc \
    ui/MyResources.qrc

DISTFILES += \
    PowerDown-VinariSoftware_es.ts
