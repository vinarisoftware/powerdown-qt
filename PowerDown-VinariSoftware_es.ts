<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>AboutWindow</name>
    <message>
        <location filename="ui/aboutwindow.ui" line="29"/>
        <source>PowerDown - Vinari Software | About</source>
        <translation>PowerDown - Vinari Software | Acerca de</translation>
    </message>
    <message>
        <location filename="ui/aboutwindow.ui" line="55"/>
        <source>PowerDown - Vinari Software is a light, multiplatform utility that allows you to schedule the shutdown or reboot of your compuer setting an exact amount of minutes before the selected action is done.

Also it allows you to cancel the scheduled action in case you need to keep using the computer.</source>
        <translation>PowerDown - Vinari Software es una ligera utilidad multiplataforma que le permite programar el apagado o el reinicio de su computador estableciendo una cantidad exacta de retraso en minutos antes de realizar la acción programada.

Y también le permite cancelar la acción que estableció en caso que necesite seguir usando el computador.</translation>
    </message>
    <message>
        <location filename="ui/aboutwindow.ui" line="81"/>
        <source>License agreement</source>
        <translation>Acuerdo de licencia</translation>
    </message>
    <message>
        <location filename="ui/aboutwindow.ui" line="88"/>
        <source>Accept</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location filename="ui/aboutwindow.ui" line="91"/>
        <source>Esc</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/aboutwindow.ui" line="118"/>
        <source>PowerDown - Vinari Software</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/aboutwindow.ui" line="149"/>
        <source>Version: 2.0.5 
All rights reserved. All logos &amp; pictures here are property of Vinari Software
And of their respective owners.
All the source code and it&apos;s binary files are licensed under the BSD-3 license.</source>
        <oldsource>Version: 2.0.4 
All rights reserved. All logos &amp; pictures here are property of Vinari Software
And of their respective owners.
All the source code and it&apos;s binary files are licensed under the BSD-3 license.</oldsource>
        <translation>Version 2.0.5
Todos los derechos reservados. Todos los logos aquí son propiedad de Vinari Software y de sus respectivos dueños.
Todo el código fuente y los archivos binarios están protegidos bajo la licencia BSD-3.</translation>
    </message>
    <message>
        <source>Version: 2.0.3
All rights reserved. All logos &amp; pictures here are property of Vinari Software
And of their respective owners.
All the source code and it&apos;s binary files are licensed under the BSD-3 license.</source>
        <oldsource>Version: 2.0.2
All rights reserved. All logos &amp; pictures here are property of Vinari Software
And of their respective owners.
All the source code and it&apos;s binary files are licensed under the BSD-3 license.</oldsource>
        <translation type="vanished">Version 2.0.3
Todos los derechos reservados. Todos los logos aquí son propiedad de Vinari Software y de sus respectivos duenos.
Todo el código fuente y los archivos binarios están protegidos bajo la licencia BSD-3.</translation>
    </message>
    <message>
        <source>Version: 2.0.1
All rights reserved. All logos &amp; pictures here are property of Vinari Software
And of their respective owners.
All the source code and it&apos;s binary files are licensed under the BSD-3 license.</source>
        <oldsource>Version: 2.0.0.2
All rights reserved. All logos &amp; pictures here are property of Vinari Software
And of their respective owners.
All the source code and it&apos;s binary files are licensed under the BSD-3 license.</oldsource>
        <translation type="vanished">Version 2.0.1
Todos los derechos reservados. Todos los logos aquí son propiedad de Vinari
Software
y de sus respectivos duenos.
Todo el código fuente y los archivos binarios están protegidos bajo la licencia
BSD-3.</translation>
    </message>
</context>
<context>
    <name>LicenseWindow</name>
    <message>
        <location filename="ui/licensewindow.ui" line="29"/>
        <source>PowerDown - Vinari Software | License</source>
        <translation>PowerDown - Vinari Software | Licencia</translation>
    </message>
    <message>
        <location filename="ui/licensewindow.ui" line="58"/>
        <source>Copyright (C) 2015 - 2025. Vinari Software.
All rights reserved.


Redistribution and use in source and binary forms, source code with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY VINARI SOFTWARE &quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL VINARI SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
</source>
        <oldsource>Copyright (C) 2015-2024. Vinari Software.
All rights reserved.


Redistribution and use in source and binary forms, source code with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY VINARI SOFTWARE &quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL VINARI SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
</oldsource>
        <translation type="unfinished">Copyright (C) 2015-2024. Vinari Software.
All rights reserved.


Redistribution and use in source and binary forms, source code with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY VINARI SOFTWARE &quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL VINARI SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
</translation>
    </message>
    <message>
        <location filename="ui/licensewindow.ui" line="81"/>
        <source>Accept</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location filename="ui/licensewindow.ui" line="84"/>
        <source>Esc</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="ui/mainwindow.ui" line="26"/>
        <source>PowerDown - Vinari Software</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="60"/>
        <source>Shutdown</source>
        <translation>Apagar</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="95"/>
        <source>Reboot</source>
        <translation>Reiniciar</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="123"/>
        <location filename="src/mainwindow.cpp" line="188"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="151"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="160"/>
        <source>&amp;About</source>
        <translation>Acerca &amp;de</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="178"/>
        <source>About PowerDown - Vinari Software</source>
        <translation>Acerca de PowerDown - Vinari Software</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="187"/>
        <source>About Vinari Software</source>
        <translation>Acerca de Vinari Software</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="196"/>
        <source>About Vinari OS</source>
        <translation>Acerca de Vinari OS</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="205"/>
        <source>About QT</source>
        <translation>Acerca de QT</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="214"/>
        <source>Settings</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="217"/>
        <source>Ctrl+,</source>
        <translation>Ctrl+,</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="226"/>
        <source>Quit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="229"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="238"/>
        <source>Enable cancel button</source>
        <translation>Activar botón cancelar</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="85"/>
        <source>PowerDown - Vinari Software | About QT</source>
        <translation>PowerDown - Vinari Software | Acerca de QT</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="90"/>
        <source>You will be redirected to the Vinari OS website 
Do you want to proceed?</source>
        <oldsource>You will be redirected to the Vinari OS website
Do you want to proceed?</oldsource>
        <translation>Será redirigido al sitio web de Vinari OS ¿Desea continuar?</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="101"/>
        <source>You will be redirected to the Vinari Software website 
Do you want to proceed?</source>
        <oldsource>You will be redirected to the Vinari Software website
Do you want to proceed?</oldsource>
        <translation>Será redirigido al sitio web de Vinari Software ¿Desea continuar?</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="113"/>
        <location filename="src/mainwindow.cpp" line="143"/>
        <source>There is already an scheduled action.
Please cancel it, and try again...</source>
        <translation>Ya existe una acción programada.
Por favor, cancele dicha acción e intente de nuevo...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="128"/>
        <source>An error has occurred while trying to shutdown the computer....</source>
        <translation>Ha ocurrido un error al intentar apagar el computador...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="132"/>
        <source>Shuting down | PowerDown - Vinari Software</source>
        <translation>Apagando | PowerDown - Vinari Software</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="133"/>
        <source>Cancel shutdown</source>
        <translation>Cancelar apagado</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="137"/>
        <source>The computer will shut down in </source>
        <translation>El computador se apagará en </translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="137"/>
        <location filename="src/mainwindow.cpp" line="167"/>
        <source> minute(s).</source>
        <translation> minuto(s).</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="158"/>
        <source>An error has occurred while trying to reboot the computer...</source>
        <translation>Ha ocurrido un error al intentar reinicial el computador...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="162"/>
        <source>Rebooting | PowerDown - Vinari Software</source>
        <translation>Reiniciando | PowerDown - Vinari Software</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="163"/>
        <source>Cancel reboot</source>
        <translation>Cancelar reinicio</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="167"/>
        <source>The computer will reboot in </source>
        <translation>El computador se reiniciará en </translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="183"/>
        <source>An error has occurred while trying to cancel the scheduled action...</source>
        <translation>Ha ocurrido un error al intentar cancelar la acción programada...</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="192"/>
        <source>The established action has been canceled successfully.</source>
        <translation>La acción establecida ha sido cancelada exitosamente.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="210"/>
        <source>Forced cancel | PowerDown - Vinari Software</source>
        <translation>Cancelar forzado | PowerDown - Vinari Software</translation>
    </message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <location filename="ui/settingswindow.ui" line="29"/>
        <source>PowerDown - Vinari Software | Settings</source>
        <translation>PowerDown - Vinari Software | Configuración</translation>
    </message>
    <message>
        <location filename="ui/settingswindow.ui" line="62"/>
        <source>Amount of time  to delay the
shutdown or reboot:</source>
        <translation>Cantidad de tiempo de retraso
para el apagado o reinicio:</translation>
    </message>
    <message>
        <location filename="ui/settingswindow.ui" line="79"/>
        <source>Remember last time set.</source>
        <translation>Recordar tiempo establecido.</translation>
    </message>
    <message>
        <location filename="ui/settingswindow.ui" line="93"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="ui/settingswindow.ui" line="100"/>
        <source>Apply changes</source>
        <translation>Aplicar cambios</translation>
    </message>
</context>
</TS>
